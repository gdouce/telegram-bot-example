require 'spec_helper'

describe Movie do
  subject(:movie) { described_class.new('Titanic', 'Won 11 Oscars. 126 wins & 83 nominations total') }

  describe 'model' do
    it { is_expected.to respond_to(:title) }
    it { is_expected.to respond_to(:awards_information) }
  end
end
