require 'spec_helper'

require "#{File.dirname(__FILE__)}/../conectors/omdb_api_conector"
require "#{File.dirname(__FILE__)}/../app/tv/movie"

describe 'omdb_api_conector' do
  let(:titanic_movie) do
    Movie.new('Titanic', 'Won 11 Oscars. 126 wins & 83 nominations total')
  end

  it 'i ask for a film i get that film' do
    # me di cuenta que este test no hace nada
    mock_object_api = instance_double('OmdbApiConector')
    allow(mock_object_api).to receive(:get_movie).with('Titanic').and_return(titanic_movie)
    movie = mock_object_api.get_movie('Titanic')
    expect(movie.title).to eq titanic_movie.title
  end

  it 'i ask for a movie that is not in the database i get was not found as the awards' do
    # me di cuenta que el test de arriba no hace nada.
    # No se como testear con mock objets la verdad
    #
  end

  # it 'I ask for movie that won no awards i get "won no awards"
  # in movies awardsInfomation atribute' do
  #   # No se como testear con mock objets la verdad
  # end
end
