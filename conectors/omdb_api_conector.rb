require 'faraday'
require "#{File.dirname(__FILE__)}/../app/tv/movie"

class OmdbApiConector
  def initialize
    @api_url = 'https://www.omdbapi.com/'
    @api_key = ENV['OMDB_API_KEY']
  end

  def get_movie(movie_name)
    response = Faraday.get(@api_url, { t: movie_name, apikey: @api_key })
    movie_in_json = JSON.parse(response.body)
    if movie_in_json['Title']
      if movie_in_json['Awards'] == 'N/A'
        Movie.new(movie_in_json['Title'], 'won no awards')
      else
        Movie.new(movie_in_json['Title'], movie_in_json['Awards'])
      end
    end
  end
end
