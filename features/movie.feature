Feature: Movie Awards
  As a movie enthusiast, I want to be able to check the awards won by a movie.

  Scenario: a1 I check the awards of a single movie
    When I check the awards of the movie "Titanic"
    Then I should receive a message indicating the awards won by that movie

  Scenario: a2 I check the awards of more than one movie
    When I check the awards of the movie "Titanic" and the movie "Casablanca"
    Then I should receive a message indicating the awards of all the found movies

  Scenario: a3 I check the awards of a movie that is not found
    When I check the awards of the movie "Titanic 3"
    Then I should receive a message indicating that the movie was not found

  Scenario: a4 I check the awards of a movie that did not win awards
    When I check the awards of the movie "Terminator"
    Then I should receive a message indicating that the movie did not win any awards