class Movie
  attr_reader :title, :awards_information

  def initialize(title, awards_information)
    @title = title
    @awards_information = awards_information
  end
end
